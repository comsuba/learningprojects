package me.subha.models

import me.subha.lib_models.Wheels
import me.subha.print
import javax.inject.Inject

class Car {

    private val wheel : Wheels
    private val engine : Engine

    @Inject constructor(wheels: Wheels,engine: Engine){
        this.engine = engine
        this.wheel = wheels
    }

    fun drive() {
        wheel.creteWheel()
        engine.runEngine()
        "Car is driving".print()
    }

    // This is an example of field injection
    @Inject
    fun enableRemote(remote: Remote){
        remote.setListener(this)
    }

}