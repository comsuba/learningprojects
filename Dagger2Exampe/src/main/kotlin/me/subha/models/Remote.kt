package me.subha.models

import me.subha.print
import javax.inject.Inject

class Remote @Inject constructor(){

    fun setListener(car: Car) = "Remote Connected".print()

}