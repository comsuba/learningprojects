package me.subha.models

import javax.inject.Inject

interface Engine {
    fun runEngine()
}

class PetrolEngine @Inject constructor() : Engine{

    override fun runEngine() {
        "Petrol Engine started"
    }
}


class DieselEngine : Engine {
    private val horsePower : Int

    constructor(horsePower : Int){
        this.horsePower = horsePower
    }
    override fun runEngine() {
        "Diesel Engine started"
    }

}