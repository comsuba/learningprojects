package me.subha.models

import me.subha.print
import javax.inject.Inject

class SampleModelForFieldInjection @Inject constructor(){
    fun isCreated() = "I am Happy".print()
}