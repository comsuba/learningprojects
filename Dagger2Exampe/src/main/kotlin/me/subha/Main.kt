package me.subha

import me.subha.di.DaggerCarComponent
import me.subha.di.DieselEngineModule
import me.subha.models.SampleModelForFieldInjection
import javax.inject.Inject

fun String.print() = println(this)


fun main() {
    Main().start()
}

class Main {

    // this is field injection
    @Inject
    lateinit var sampleModelForFieldInjection : SampleModelForFieldInjection

    fun start(){
        val carComponent = DaggerCarComponent.builder().dieselEngineModule(DieselEngineModule(100)).build()
        carComponent.injectMain(this)
        val car = carComponent.getCar()
        car.drive()

        sampleModelForFieldInjection.isCreated()

    }
}


