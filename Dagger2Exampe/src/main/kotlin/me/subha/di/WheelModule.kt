package me.subha.di

import dagger.Module
import dagger.Provides
import me.subha.lib_models.Rims
import me.subha.lib_models.Tires
import me.subha.lib_models.Wheels

@Module
object WheelModule {

    @JvmStatic @Provides fun provideRim() = Rims.getInstance()
    @JvmStatic @Provides fun provideTires() = Tires.getInstance()
    @JvmStatic @Provides fun provideWheel(rims: Rims, tires: Tires) = Wheels.getInstance(rims,tires)

}