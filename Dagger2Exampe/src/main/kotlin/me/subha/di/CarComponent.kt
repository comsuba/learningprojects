package me.subha.di

import dagger.Component
import me.subha.Main
import me.subha.models.Car

@Component (modules = [WheelModule::class,DieselEngineModule::class])
interface CarComponent {
    fun getCar() : Car
    fun injectMain(main : Main)
}