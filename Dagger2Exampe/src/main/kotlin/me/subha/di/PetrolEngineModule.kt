package me.subha.di

import dagger.Binds
import dagger.Module
import me.subha.models.Engine
import me.subha.models.PetrolEngine

@Module
abstract class PetrolEngineModule {
    @Binds
    abstract fun bindPetrolEngine(petrolEngine: PetrolEngine) : Engine
}