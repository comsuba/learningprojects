package me.subha.di

import dagger.Module
import dagger.Provides
import me.subha.models.DieselEngine
import me.subha.models.Engine

@Module
class DieselEngineModule(private val horsePower : Int) {

    @Provides
    fun bindPetrolEngine() : Engine {
        return DieselEngine(horsePower)
    }
}