package me.subha.lib_models

import me.subha.print

class Tires {

    companion object{
        @Volatile private var tires : Tires? = null

        fun getInstance() : Tires {
            val mTires = tires
            if(mTires != null){
                return mTires
            }

            return synchronized(this){
                if(mTires != null){
                    mTires
                }else{
                    val newTires = Tires()
                    tires = newTires
                    newTires
                }
            }
        }
    }

    fun inflateTire(){
        "Tires inflated".print()
    }

}