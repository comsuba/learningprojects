package me.subha.lib_models

import me.subha.print

class Rims {

    companion object {
        @Volatile private var rims : Rims? = null

        fun getInstance() : Rims {
            val mRims = rims
            if(mRims != null){
                return mRims
            }

            return synchronized(this){
                if(mRims != null){
                    mRims
                }else{
                    val newRims = Rims()
                    rims = newRims
                    newRims
                }
            }
        }
    }

    fun createRim(){
        "Rim Created".print()
    }

}