package me.subha.lib_models

import me.subha.print

class Wheels private constructor(private val rims: Rims, private val tires: Tires) {

    companion object{
        @Volatile private var wheels : Wheels? = null

        fun getInstance(rims: Rims,tires: Tires) : Wheels {
            val mWheels = wheels
            if(mWheels != null){
                return mWheels
            }

            return synchronized(this){
                if(mWheels != null){
                    mWheels
                }else{
                    val newWheels = Wheels(rims,tires)
                    wheels = newWheels
                    newWheels
                }
            }
        }
    }

    fun creteWheel(){
        rims.createRim()
        tires.inflateTire()
        "Wheel Created".print()
    }

}